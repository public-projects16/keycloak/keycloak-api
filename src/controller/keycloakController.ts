import { strategy } from "../auth/keycloakAuth";
import passport from "passport";

export class KeycloakController {
    constructor() {
        passport.use(strategy);
        passport.authenticate("keycloak", {session: false});
    }
}