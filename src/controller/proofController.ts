import { Request, Response, NextFunction } from "express";
import { JWT } from "../utils/jwt";
import passport from "passport";

class ProofController {

    public index(req: Request, res: Response) {
        res.json({msg: "This is a proof using keycloak"});
    }

    public loggedIn(req: Request, res: Response) {
        passport.authenticate("keycloak" , {session: false});
        const token: any = req.headers.authorization;
        const decoded_token: any = JWT.decodeHeaderToken(token);
        res.json({status: "success", data: { info: "You´re logged in using keycloak<3", name: decoded_token.name, mail: decoded_token.email }  , err: {}});
    }
}

const proofController: ProofController = new ProofController();
export default proofController;