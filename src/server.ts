import express, { Application } from "express";
import cors from "cors";
import * as dotenv from "dotenv";
import proofRoutes from "./routes/proofRoutes";
import passport from "passport";
import { KeycloakController } from "./controller/keycloakController";

dotenv.config();

class Server {

    private app: Application;
    public authController: KeycloakController;

    constructor() {
        this.app = express();
        this.configuration();
        this.authController = new KeycloakController();
        this.routes();
    }

    private configuration() {
        this.app.set("port", process.env.PORT || 3000);
        this.app.use(cors({origin: true}));
        this.app.use(express.json());
        this.app.use(passport.initialize());
    }

    private routes() {
        this.app.use(proofRoutes);
    }

    start(): void {
        this.app.listen( this.app.get("port"), () => {
            console.log("Running on port %d", this.app.get("port"));
            console.log("Press CTRL + C to stop it");
        });
    }
}

const server: Server = new Server();
export default server;