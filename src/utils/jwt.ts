import * as  jwt from 'jsonwebtoken';


export class JWT {
    constructor() {}

    static decodeHeaderToken(header: string) {
        const token = header.split(" ")[1];
        return  jwt.decode(token);
    }
}