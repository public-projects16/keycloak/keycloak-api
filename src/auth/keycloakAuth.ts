var KeycloakStrategy = require("passport-keycloak-bearer");
import passport from "passport";

const configuration = {
    "realm": "demo",
    "url": "http://localhost:8080/auth"
}
const strategy = new KeycloakStrategy(configuration, (jwtPayload: any, done: any) => {
    return done(null, jwtPayload);
});

passport.use(strategy);

export { strategy }