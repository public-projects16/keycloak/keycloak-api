import { Router } from "express";
import proofController from "../controller/proofController";
import passport from "passport";

class ProofRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }
    config(): void {
        this.router.get("/",  proofController.index);
        this.router.get("/access", passport.authenticate("keycloak" , {session: false}), proofController.loggedIn);
    }
}

const proofRoutes = new ProofRoutes();
export default proofRoutes.router;