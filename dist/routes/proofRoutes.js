"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const proofController_1 = __importDefault(require("../controller/proofController"));
const passport_1 = __importDefault(require("passport"));
class ProofRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get("/", proofController_1.default.index);
        this.router.get("/access", passport_1.default.authenticate("keycloak", { session: false }), proofController_1.default.loggedIn);
    }
}
const proofRoutes = new ProofRoutes();
exports.default = proofRoutes.router;
