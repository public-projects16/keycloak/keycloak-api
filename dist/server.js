"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const dotenv = __importStar(require("dotenv"));
const proofRoutes_1 = __importDefault(require("./routes/proofRoutes"));
const passport_1 = __importDefault(require("passport"));
const keycloakController_1 = require("./controller/keycloakController");
dotenv.config();
class Server {
    constructor() {
        this.app = express_1.default();
        this.configuration();
        this.authController = new keycloakController_1.KeycloakController();
        this.routes();
    }
    configuration() {
        this.app.set("port", process.env.PORT || 3000);
        this.app.use(cors_1.default({ origin: true }));
        this.app.use(express_1.default.json());
        this.app.use(passport_1.default.initialize());
    }
    routes() {
        this.app.use(proofRoutes_1.default);
    }
    start() {
        this.app.listen(this.app.get("port"), () => {
            console.log("Running on port %d", this.app.get("port"));
            console.log("Press CTRL + C to stop it");
        });
    }
}
const server = new Server();
exports.default = server;
