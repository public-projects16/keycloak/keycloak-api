"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jwt_1 = require("../utils/jwt");
const passport_1 = __importDefault(require("passport"));
class ProofController {
    index(req, res) {
        res.json({ msg: "This is a proof using keycloak" });
    }
    loggedIn(req, res) {
        passport_1.default.authenticate("keycloak", { session: false });
        const token = req.headers.authorization;
        const decoded_token = jwt_1.JWT.decodeHeaderToken(token);
        res.json({ status: "success", data: { info: "You´re logged in using keycloak<3", name: decoded_token.name, mail: decoded_token.email }, err: {} });
    }
}
const proofController = new ProofController();
exports.default = proofController;
