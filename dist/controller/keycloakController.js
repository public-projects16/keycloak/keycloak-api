"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const keycloakAuth_1 = require("../auth/keycloakAuth");
const passport_1 = __importDefault(require("passport"));
class KeycloakController {
    constructor() {
        passport_1.default.use(keycloakAuth_1.strategy);
        passport_1.default.authenticate("keycloak", { session: false });
    }
}
exports.KeycloakController = KeycloakController;
