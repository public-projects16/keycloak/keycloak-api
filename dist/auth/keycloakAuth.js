"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var KeycloakStrategy = require("passport-keycloak-bearer");
const passport_1 = __importDefault(require("passport"));
const configuration = {
    "realm": "demo",
    "url": "http://localhost:8080/auth"
};
const strategy = new KeycloakStrategy(configuration, (jwtPayload, done) => {
    return done(null, jwtPayload);
});
exports.strategy = strategy;
passport_1.default.use(strategy);
